# Qubik Listener

Listen for frames from the flowgraph via UDP and forward them to satnogs-db.

## Installation

Create a python virtual environment and install the dependencies inside:
```
mkvirtualenv -p python3 qubik_listener
pip install -r requirements.txt
```

Dependencies:
- [satnogs-decoders](https://gitlab.com/librespacefoundation/satnogs/satnogs-decoders) for the (Qubik) frame structure
- [kaitaistruct](https://pypi.org/project/kaitaistruct/) for unpacking the frames


## Usage

```
usage: qubik_listener.py [-h] [--callsign CALLSIGN] [--lat LAT] [--lon LON] [--udp_ip UDP_IP] [--udp_port UDP_PORT]
                         [--telemetry_url TELEMETRY_URL]

Listen for frames via UDP and forward them to satnogs-db.

optional arguments:
  -h, --help            show this help message and exit
  --callsign CALLSIGN   Callsign, e.g. ZZ0ZZZ
  --lat LAT             Latitude, e.g. 23.50000E
  --lon LON             Longitude, e.g. 53.25000N
  --udp_ip UDP_IP       GRC UDP IP, e.g. 127.0.0.1
  --udp_port UDP_PORT   GRC UDP Port, e.g. 16982
  --telemetry_url TELEMETRY_URL
                        SatNOGS DB API Telementry endpoint, e.g. https://db-dev.satnogs.org/api/telemetry/ ; default: ""
```

## Docker

An example Compose file is included in this repository.
Edit `docker-compose.yml` and uncomment the appropriate predefined environment variables file entries.
To pull and bring up the service use `docker-compose pull && docker-compose up -d`.
If you want to build and bring up your own images use `docker-compose up --build -d`.

## Changelog

### 2021-11-05
- Added support for Qubik-3 to -6 using the SANA-assigned spacecraft ids (& the spacecraft ids 3 to 6 as fallback)
- Removed commandline arguments for norad ids (hardcoded for Qubik-1 to -6 now)
- Disabled telemetry forwarding by default. The `telemetry_url` argument has to be set to a valid value to enable forwarding

### 2021-11-03
- Added support for Qubik-3

## Testing (without a flowgraph)

You can use the following command to periodically send a static packet via UDP (instead of using your own satellite, station & flowgraph):
```
watch ./qubik_send_frame.py
```

## License

[![license](https://img.shields.io/badge/license-AGPL%203.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202014--2020-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)
